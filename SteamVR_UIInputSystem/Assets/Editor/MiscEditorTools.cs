﻿using UnityEngine;
using UnityEditor;

public class MiscEditorTools
{
	[MenuItem("Arklay/Clear PlayerPrefs")]
	private static void ClearPlayerPrefs()
	{
		PlayerPrefs.DeleteAll();
	}

    [MenuItem("Arklay/Clear Cached Assets")]
	private static void ClearCachedAssets()
	{
        if (Caching.CleanCache())
        {
            Debug.Log("Clean cache success");
        }
        else
        {
            Debug.LogWarning("Clean Cache failed, please try again (cache in use)");
        }
	}
}